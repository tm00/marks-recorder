package marks;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class FileIOManager {

	
	private static String fileDirectory=System.getProperty("user.dir");
	private static final String settingsFileName = "Settings.set";
	


	//-----------
	//Database
	//----------
	public static void writeSettings(){
		try{

			File f = new File(fileDirectory+"\\"+settingsFileName);
			System.out.println(f.getPath());
			FileOutputStream fout = new FileOutputStream(f);

			ObjectOutputStream oos = new ObjectOutputStream(fout); 

			//Create new database without historic data...
			Settings newdata = new Settings();
						
			oos.writeObject(newdata);

			oos.close();
			System.out.println("Done");

		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public static void readSettings(){
		Settings ingelees;
		try{
			File f = new File(fileDirectory+"/"+settingsFileName);
			FileInputStream fin = new FileInputStream(f);
			ObjectInputStream ois = new ObjectInputStream(fin);
			ingelees = (Settings) ois.readObject();

			Settings.initialsColumnLetter=ingelees.initialsColumnLetter;
			Settings.surnameColumnLetter=ingelees.surnameColumnLetter;
			Settings.studentNumberColumnLetter=ingelees.studentNumberColumnLetter;

			ois.close();

		}catch(Exception ex){
			ex.printStackTrace();
			System.out.println("Settings not found, revertin to default");
			
		} 
	}
	
	public static void main(String[] args) {
//		System.out.println((new Settings()).toString());
		
		readSettings();
		System.out.println("after read: "+(new Settings()).toString());

		
		Settings.studentNumberColumnLetter="G";
		Settings.markColumnLetter="F";
		System.out.println("b4 write: "+(new Settings()).toString());

		writeSettings();
	}
}
