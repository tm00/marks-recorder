package marks;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MarksController implements TextOutput{

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private AnchorPane mainPane;

	@FXML
	private TextField markText;

	@FXML
	private VBox hBoxContainer;

	@FXML
	private Button chooseFileButton;

	@FXML
	private TextArea infoText;

	@FXML
	private MenuBar menu;

	private static String filePath;
	private static StudentList list;
	//	private final AutoFillTextBox<String> fillbox = new AutoFillTextBox<String>();
	//	private final AutoCompleteTextField autoTex = new AutoCompleteTextField();
	private final AutoCompleteTextField2 autoTex = new AutoCompleteTextField2();
	public static URL settingsResource;


	@FXML
	void chooseFilePressed(ActionEvent event) {
		/*fillbox.setListLimit(10);

		fillbox.setFilterMode(true);

		fillbox.setMinWidth(hBoxContainer.getWidth());
		hBoxContainer.getChildren().addAll(fillbox);*/

		//		hBoxContainer.getChildren().addAll(autoTex);


		File f = ExcelFileHandler.getFile();
		chooseFileButton.setText(f.getName());
		
		filePath = f.getAbsolutePath();

		ExcelFileHandler.setFilePath_to_excel(filePath);
		ExcelFileHandler.loadConstantsFromSettings();


		list=ExcelFileHandler.readStudentListFromFile();

		//		fillbox.setData(list.getObservableList());
		autoTex.getEntries().addAll(list.getObservableList());
		autoTex.setCaseSensitive(false);
	}



	@FXML
	void addMarkPressed(ActionEvent event) {



		//		String selected = fillbox.getText();
		String selected = autoTex.getText();
		list.getStudent(selected).setMark(Double.parseDouble(this.markText.getText()));

		autoTex.setText("");

		autoTex.requestFocus();

	}

	@FXML
	void writeMarks(ActionEvent event) {

		ExcelFileHandler.writeStudentMarksToFile(list);

		System.out.println("DONE!!");
		this.displayText("Succesfully written to the file...",true);
	}

	@FXML
	void initialize() {
		assert markText != null : "fx:id=\"markText\" was not injected: check your FXML file 'Gui.fxml'.";
		assert hBoxContainer != null : "fx:id=\"hBoxContainer\" was not injected: check your FXML file 'Gui.fxml'.";


		hBoxContainer.getChildren().addAll(autoTex);
		markText.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent keyEvent) {
				if (keyEvent.getCode() == KeyCode.ENTER)  {
					addMarkPressed(null);
				}
			}
		});

		//File Dragging functionality...  START


		mainPane.setOnDragOver(new EventHandler<DragEvent>(){

			@Override
			public void handle(DragEvent event) {
				event.acceptTransferModes(TransferMode.COPY);
				event.consume();

			}
		});

		mainPane.setOnDragEntered(new EventHandler<DragEvent>(){

			@Override
			public void handle(DragEvent event) {

				event.consume();

			}
		});

		mainPane.setOnDragExited(new EventHandler<DragEvent>(){

			@Override
			public void handle(DragEvent event) {

				//				String s = infoTXT.getText();
				//				infoTXT.setText(s+"\n Drop finished\n");

				event.consume();

			}
		});


		mainPane.setOnDragDropped(new EventHandler<DragEvent>() {

			@Override
			public void handle(DragEvent event) {
				Dragboard db = event.getDragboard();
				boolean success = false;
				if (db.hasFiles()) {
					success = true;

					if(db.getFiles().size()==1){
						File file = db.getFiles().get(0);
						chooseFileButton.setText(file.getName());
						filePath= file.getAbsolutePath();
						System.out.println(filePath);

						ExcelFileHandler.setFilePath_to_excel(filePath);

						ExcelFileHandler.loadConstantsFromSettings();


						list=ExcelFileHandler.readStudentListFromFile();

						//						fillbox.setData(list.getObservableList());
						autoTex.getEntries().addAll(list.getObservableList());
						autoTex.setCaseSensitive(false);

					}

				}
				event.setDropCompleted(success);
				event.consume();
			}
		});



		//File Dragging functionality...  END

		this.infoText.setFocusTraversable(false);
		this.chooseFileButton.setFocusTraversable(false);
		this.autoTex.requestFocus();


		Student.outText=this;

		String def  = "Welcome to the mark logging program!!\n"
				+ "This program lets you log marks to an excel file\n"
				+ "HOW IT WORKS: You first need to create the column you want the marks to be entered in and specify this column in Settings. "
				+ "Then close the excel file. Drag and drop the selected excel file to which the marks should be added. "
				+ "The student details are now read by the program. You may continue to enter the student name/number "
				+ "and select one of the suggestions by pressing the ENTER key. \n"
				+ "After adding the marks, press the \" Write to file\" button to export the newly added student data to the"
				+ "excel file.\n"
				+ "FYI the excel file should be in a specific format such as the one from the example. Additional settings can be set "
				+ "by using the settings menu. \n\n";
		this.infoText.setText(def);
		menuBarStuff();
	}

	private void menuBarStuff(){
		ObservableList<Menu> menus=menu.getMenus();
		menus.clear();

		Menu filemenu = new Menu("File");

		MenuItem chooseRoot = new MenuItem("Settings");
		chooseRoot.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				FXMLLoader fxmlLoader = new FXMLLoader();
				System.out.println("ResourcePath = "+getClass().getResource("SettingsGUI.fxml"));
				fxmlLoader.setLocation(settingsResource);
				AnchorPane page = null;

				try {
					page = fxmlLoader.load();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				SettingsController mc = (SettingsController) fxmlLoader.getController();

				Stage stage = Main.primaryStage;//new Stage(StageStyle.DECORATED);
				Scene scene = new Scene(page);
				scene.getStylesheets().add(this.getClass().getResource("application.css").toExternalForm());	
				stage.setScene(scene); 


				mc.startOFF();
				stage.show();
			}
		});


		MenuItem exit = new MenuItem("Exit");
		exit.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				Platform.exit();
			}

		});
		filemenu.getItems().addAll(chooseRoot,exit);

		menu.getMenus().addAll(filemenu);

	}


	@Override
	public void displayText(String text,boolean doublenl) {
		infoText.appendText(text+"\n");	
		
		if(doublenl){
			infoText.appendText("\n");
		}
	}


}
