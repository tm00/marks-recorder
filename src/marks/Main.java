package marks;

import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import res.ResourceLoader;

public class Main extends Application {

	public static Stage primaryStage;
	public static URL resourcePath;

	@FXML public AnchorPane content;
	@Override
	public void start(Stage primaryStage) {
		try {

			/*
			 GENERAL COMMENTS: 


			  clear box after mark has been added

			  Enter button should trigger add mark when the mark input has focus

			  column index inputs should be replaced by eg "A" or "G" column letters of file 	
			  				- Added in filehandler ... please add to GUI, maybe a setting or a popup? or in GUI might be better
			 	NB how to read static settings???
			 	maybe create a more dynamic settings Gui by using a list of settings? then create
			 		a Hbox class or someting to display each one and update using update button??



			 - Control where the focus goes after entering/adding the mark, eg to the name textfield for the next entry
			 */
			Main.primaryStage=primaryStage;
			//resourcePath=Main.class.getResource("Gui.fxml");
			MarksController.settingsResource=Main.class.getResource("SettingsGui.fxml");
			System.out.println("ResourcePath in main = "+Main.class.getResource("Gui.fxml"));
			AnchorPane page = (AnchorPane) FXMLLoader.load(Main.class.getResource("Gui.fxml"));
			//			MarksController mc = (MarksController) fxmlLoader.getController();
			Scene scene = new Scene(page);
			scene.getStylesheets().add(this.getClass().getResource("application.css").toExternalForm());
			primaryStage.setResizable(false);
			primaryStage.setScene(scene);
			primaryStage.setTitle("Marks adding");
			primaryStage.getIcons().add(new Image(ResourceLoader.getFileInputStream("16x16.png")));
			primaryStage.getIcons().add(new Image(ResourceLoader.getFileInputStream("24x24.png")));
			primaryStage.getIcons().add(new Image(ResourceLoader.getFileInputStream("32x32.png")));
			primaryStage.getIcons().add(new Image(ResourceLoader.getFileInputStream("48x48.png")));
			primaryStage.getIcons().add(new Image(ResourceLoader.getFileInputStream("64x64.png")));
			primaryStage.getIcons().add(new Image(ResourceLoader.getFileInputStream("96x96.png")));
			primaryStage.getIcons().add(new Image(ResourceLoader.getFileInputStream("128x128.png")));
			primaryStage.getIcons().add(new Image(ResourceLoader.getFileInputStream("256x256.png")));
			primaryStage.getIcons().add(new Image(ResourceLoader.getFileInputStream("512x512.png")));
			//			Student.outText=mc;
			primaryStage.show();
			Settings.setDefaults();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {

		launch(args);

	}
}
