package marks;

import java.io.Serializable;

public class Settings implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	//Excel file Settings
	public static String initialsColumnLetter;
	public static String surnameColumnLetter; 
	public static String studentNumberColumnLetter;
	public static String markColumnLetter;
	public static int startRowIndex;

	public String toString(){
		String s = "Settings as it stands:";
		s+="\n initial Letter = "+initialsColumnLetter;
		s+="\n surname Letter = "+surnameColumnLetter;
		s+="\n stdnumber Letter = "+studentNumberColumnLetter;
		s+="\n mark Letter = "+markColumnLetter;

		return s;
	}

	public static void setDefaults(){
		initialsColumnLetter = "C";
		surnameColumnLetter = "B";
		studentNumberColumnLetter = "A";
		markColumnLetter = "P";
		startRowIndex=2;
	}


}
