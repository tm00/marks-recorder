package marks;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class SettingsController {

	@FXML
	private ResourceBundle resources;

	@FXML
	private URL location;

	@FXML
	private TextField initials;

	@FXML
	private TextField surname;

	@FXML
	private TextField rowIndex;

	@FXML
	private TextField mark;

	@FXML
	private TextField stNum;
	
 


	@FXML
	void SaveAndCloseButtonPressed(ActionEvent event) {

		Settings.initialsColumnLetter=initials.getText();
		Settings.surnameColumnLetter=surname.getText();
		Settings.studentNumberColumnLetter=stNum.getText();
		Settings.markColumnLetter=mark.getText();
		Settings.startRowIndex=Integer.parseInt(rowIndex.getText());

		
		// NOW RETURN TO MAIN GUI
		
		try {


			FXMLLoader fxmlLoader = new FXMLLoader();
			fxmlLoader.setLocation(getClass().getResource("Gui.fxml"));
			AnchorPane page = null;

			page = fxmlLoader.load();
			MarksController mc = (MarksController) fxmlLoader.getController();
			/*		
			FileHandler.fileDirectory="C:\\Users\\TM_user\\Desktop";
			Data data = FileHandler.readData();
			if(data==null){
				System.out.println("data is hier null");
			}
			MainController.data=data;*/

			Stage primaryStage= Main.primaryStage;


			Scene scene = new Scene(page);
			scene.getStylesheets().add(this.getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			//            primaryStage.setTitle("Weerstasie");
			//            primaryStage.getIcons().add(new Image(ResourceLoader.getFileInputStream("raindrop.png")));

			//            primaryStage.setResizable(false);
			primaryStage.show();


//			mc.startOFF();
		} catch(Exception e) {
			e.printStackTrace();
		}

	}

	@FXML
	void initialize() {
		assert initials != null : "fx:id=\"initials\" was not injected: check your FXML file 'SettingsGui.fxml'.";
		assert surname != null : "fx:id=\"surname\" was not injected: check your FXML file 'SettingsGui.fxml'.";
		assert rowIndex != null : "fx:id=\"rowIndex\" was not injected: check your FXML file 'SettingsGui.fxml'.";
		assert mark != null : "fx:id=\"mark\" was not injected: check your FXML file 'SettingsGui.fxml'.";
		assert stNum != null : "fx:id=\"stNum\" was not injected: check your FXML file 'SettingsGui.fxml'.";

	}

	public void startOFF() {

		initials.setText(Settings.initialsColumnLetter);
		surname.setText(Settings.surnameColumnLetter);
		stNum.setText(Settings.studentNumberColumnLetter);
		mark.setText(Settings.markColumnLetter);
		rowIndex.setText(Settings.startRowIndex+"");

		
	}
}
