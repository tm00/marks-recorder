package main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class FEMReader {


	public static void read(){

		String INfilepath="E:\\FEM\\Tuts\\Tut05\\gravity.xlsx";
		String OUTfilepath="E:\\FEM\\Tuts\\Tut05\\Book1.xlsx";
		ArrayList<FEMData> data = new ArrayList<FEMData>(); 

		String columnHead="AB_U1";
		int sheet = 0;

		File readfileFile = new File(INfilepath);

		try {
			FileInputStream fis = new FileInputStream(readfileFile);

			XSSFWorkbook myWorkBook = new XSSFWorkbook (fis);

			XSSFSheet mySheet = myWorkBook.getSheetAt(sheet); // Get iterator to all the rows in current sheet Iterator<Row> rowIterator = mySheet.iterator(); // Traversing over each row of XLSX file while (rowIterator.hasNext()) { Row row = rowIterator.next();

			Row row;


			row=mySheet.getRow(2);

			int nullcounter =0;
			Cell headc;
			FEMData femdata=new FEMData();
			for(int cells=0;;cells++){
				headc=row.getCell(cells);
				if(headc == null){
					System.out.println("headC = null");
					nullcounter++;
					if(nullcounter == 4){
						break;
					}
					continue;
				}
				nullcounter=0;


//				femdata=new FEMData();
				System.out.println("headC = "+headc.getStringCellValue());

				if(headc.getStringCellValue().equals("X")){
					// read X values
					int ct=0;
					while(true){
						Cell newC =  mySheet.getRow(4+ct).getCell(cells);

						if(newC==null){
							break;
						}
						System.out.println("adding x value: "+newC.getNumericCellValue());
						femdata.Xvals.add(newC.getNumericCellValue());

						ct++;
					}
				}
				else if(headc.getStringCellValue().equals(columnHead)){
					// read values values
					int ct=0;
					while(true){
						Cell newC =  mySheet.getRow(4+ct).getCell(cells);

						if(newC==null){
							break;
						}

						try{
							System.out.println("adding y value: "+newC.getNumericCellValue());
							femdata.Yvals.add(newC.getNumericCellValue());

						}catch(Exception e){
							e.printStackTrace();

						}

						ct++;
					}
					femdata.print();
					data.add(femdata);
					femdata=new FEMData();
				}
			}


			myWorkBook.close();
			// NOW WRITE THE DATA
			File outfileFile = new File(OUTfilepath);
			fis = new FileInputStream(outfileFile);

			myWorkBook = new XSSFWorkbook (fis);

			mySheet = myWorkBook.getSheetAt(sheet);

			int startrow = 2;


			int colcounter=1;
			int rowcounter=startrow+1;
			for (FEMData femData2 : data) {

				for (int i=0; i<femData2.Xvals.size() ; i++) {

					mySheet.createRow(rowcounter).createCell(colcounter).setCellValue(femData2.Xvals.get(i));
					mySheet.createRow(rowcounter).createCell(colcounter+1).setCellValue(femData2.Xvals.get(i));
				}
				colcounter+=3;
			}

			fis.close();
			FileOutputStream fileOutput;
			fileOutput = new FileOutputStream(outfileFile);
			myWorkBook.write(fileOutput);
			fileOutput.close();
			myWorkBook.close();


		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // Finds the workbook instance for XLSX file XSSFWorkbook myWorkBook = new XSSFWorkbook (fis); // Return first sheet from the XLSX workbook XSSFSheet mySheet = myWorkBook.getSheetAt(0);
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		FEMReader.read();
		System.out.println("DONE!!");

	}

}
