package main;

import java.util.ArrayList;

public class FEMData {

	
	public String name;
	public ArrayList<Double> Xvals = new ArrayList<Double>(); 
	public ArrayList<Double> Yvals = new ArrayList<Double>(); 
	
	public void print(){
		
		if(Xvals.size()!=Yvals.size()){
			System.out.println("arrays not equal");
		}
		
		for (int i = 0; i < Xvals.size(); i++) {
			try{
			System.out.println("   point x= "+Xvals.get(i)+" y= "+Yvals.get(i));
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
}
