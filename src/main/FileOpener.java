package main;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class FileOpener {

	/**
	 * Method that opens the file from the provided path with notepad++ (if installed)
	 * @param filePath
	 */
	public static void openFileWithNotepadPP(String filePath){

		//Path to notepad++.exe
		String notePadppexe = "C:\\Program Files (x86)\\Notepad++\\notepad++.exe";

		Runtime runtime = Runtime.getRuntime();
		try {
			runtime.exec(notePadppexe+" "+filePath);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	/**Method to open all the files within a certain folder with notepad++
	 * 
	 * @param filePath - Path to folder
	 */
	public static void openFilesFromFolderWithNotepadPP(String filePath){

		File folder = new File(filePath);

		if(folder.exists()){
			if(folder.isDirectory()){
				for(File f:folder.listFiles()){
					openFileWithNotepadPP(f.getPath());

					try {
						TimeUnit.MILLISECONDS.sleep(300); //Pause for 300 ms
					} catch (InterruptedException e) {
						e.printStackTrace();
					} 
				}
			}
			else{
				System.err.println("Not a directory... path: "+filePath);
			}
		}
		else{
			System.err.println("Folder doesn't exist.. path: "+filePath);
		}
	}

	/**
	 * Method to open the files from subfolders within a parent folder. ex a folder containing folders with with student numbers.
	 * @param filePath - Path to parent folder
	 */
	public static void openFilesFromFolderWitSubFoldersWithNotepadPP(String filePath){
		File folder = new File(filePath);

		if(folder.exists()){
			if(folder.isDirectory()){
				for(File f:folder.listFiles()){
					openFilesFromFolderWithNotepadPP(f.getPath());

					try {
						TimeUnit.MILLISECONDS.sleep(300); //Pause for 300 ms
					} catch (InterruptedException e) {
						e.printStackTrace();
					} 
				}
			}
			else{
				System.err.println("Not a directory... path: "+filePath);
			}
		}
		else{
			System.err.println("Folder doesn't exist.. path: "+filePath);
		}
	}
}

