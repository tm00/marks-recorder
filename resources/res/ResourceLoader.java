package res;

import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;

import javafx.scene.image.Image;

public class ResourceLoader {

	private static ResourceLoader rl = new ResourceLoader();

	/*public static Image getImage(String s){

		rl.getClass().
		return Toolkit.getDefaultToolkit().getImage(rl.getClass().getResource("/"+s));
	}*/

	public static InputStream getInputStream(String s){
		return rl.getClass().getResourceAsStream("/"+s);
	}

	public static String getFilePath(String s){
		System.out.println(rl.getClass().getResource("/"+s).getPath());
		return rl.getClass().getResource("/"+s).getFile();
	}

	public static String getFiledir(String s){
		String k= rl.getClass().getResource("/").getPath();//.replace('/', '\\');
		k+=s;
		if(k.startsWith("\\")){
			//k=k.substring(1);
		}
		System.out.println(k);
		return k;
	}


	/*private String swapslashes(String k){
		return k.replace('/', '\\');
	}*/

	public static BufferedReader getFileBufferedReader(String s){
		InputStreamReader stream = new InputStreamReader(rl.getClass().getResourceAsStream("/"+s));
		return new BufferedReader(stream);
	}

	public static InputStream getFileInputStream(String s){
		InputStream stream = (rl.getClass().getResourceAsStream("/"+s));
		return stream;
	}

	public static FileOutputStream getFileOuputStream(String s){
		Path p= Paths.get(s); 
				
		File f = new File(p.toString());
		System.out.println(f.getPath());
		FileOutputStream out;
		try {
			out = new FileOutputStream(f);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return out;
	}

	public static File getFile(String s){
		String userDirectory = System.getProperty("user.dir");
		//System.out.println(userDirectory); 

		return new File(userDirectory+"\\"+s);
	}
}
