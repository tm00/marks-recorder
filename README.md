# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

This is a little program that I wrote to help me with the logging of student tut test marks to an excel file. At this stage the excel file needs to be in a specific format with the column where the marks must be entered already created. The program can now run and the column letter must be specified. An autocomplete textfield allows for easy selecting the correct student and entering his/her mark. After all the required marks are filled in, the newly added data can be written to the file by the press of a button.

* Version 0.9.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

Everything can be used as is. The dependency .jar files are provided in the JARS folder. the "main" package is an additional one for retrieving student numbers for email addresses from the excel file.

* Configuration
* Dependencies

Given in the project itself in a folder named "JARS". Just add them to the projects build path.

* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner

TM00 email: theomuller00@gmail.com